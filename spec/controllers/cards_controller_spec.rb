require 'rails_helper'

RSpec.describe CardsController, type: :controller do
    # let(:user) { create :user, password: '123123123' }
    # let(:token) { token_generator(user.id) }

    let(:cards) do
      [
        attributes_for(:card, word: 'la sombra', translation: 'the shadow'),
        attributes_for(:card, word: 'el sol', translation: 'the sun')

      ]
    end

    let(:valid_attributes) do
      {
        word: 'el solo',
        translation: 'shadow'
        # cards_attributes: cards
      }
    end

    let(:invalid_attributes) do
      {
        word: nil,
        translation: 'nil'
        # cards_attributes: cards
      }
    end

    let(:valid_session) { {} }

    describe 'Get #index' do
      it 'returns a success response' do
        Card.create! valid_attributes
        get :index, params: {}
        expect(response).to be_successful
      end
    end

    describe 'GET #show' do
      it 'returns a success response' do
        card = Card.create! valid_attributes
        get :show, params: { id: card.to_param }
        expect(response).to be_successful
      end
    end

    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new card' do
          expect {
            post :create, params: { card: valid_attributes }
          }.to change(Card, :count).by(1)
        end

        it 'renders a JSON response with the new card' do
          post :create, params: { card: valid_attributes }
          expect(response).to have_http_status(:created)
          expect(response.content_type).to eq('application/json')
          expect(response.location).to eq(card_url(Card.last))
        end
      end

      context 'with invalid params' do
        it 'renders a JSON response with errors for the new card' do
          post :create, params: { card: invalid_attributes }
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.content_type).to eq('application/json')
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          {
            word: 'el solo'
          }
        end

        it 'updates the requested card' do
          # print valid_attributes
          card = Card.create! valid_attributes
          put :update, params: { id: card.to_param, card: new_attributes }
          card.reload
          expect(card.word).to eq 'el solo'
        end

        it 'renders a JSON response with the card' do
          card = Card.create! valid_attributes

          put :update, params: { id: card.to_param, card: valid_attributes }
          expect(response).to have_http_status(:ok)
          expect(response.content_type).to eq('application/json')
        end
      end

      context 'with invalid params' do
        it 'renders a JSON response with errors for the card' do
          card = Card.create! valid_attributes

          put :update, params: { id: card.to_param, card: invalid_attributes }
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.content_type).to eq('application/json')
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested card' do
        card = Card.create! valid_attributes
        expect {
          delete :destroy, params: { id: card.to_param }
        }.to change(Card, :count).by(-1)
      end
    end


end
