
require 'rails_helper'

RSpec.describe DecksController, type: :controller do
  # let(:user) { create :user, password: '123123123' }
  # let(:token) { token_generator(user.id) }

  let(:cards) do
    [
      attributes_for(:card, word: 'la sombra', translation: 'the shadow'),
      attributes_for(:card, word: 'el sol', translation: 'the sun')

    ]
  end

  let(:valid_attributes) do
    {
      name: 'Spanish Updated',
      cards_attributes: cards
    }
  end

  let(:invalid_attributes) do
    {
      name: nil,
      cards_attributes: cards
    }
  end

  let(:valid_session) { {} }

  describe 'Get #index' do
    it 'returns a success response' do
      Deck.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      deck = Deck.create! valid_attributes
      get :show, params: { id: deck.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Deck' do
        expect {
          post :create, params: { deck: valid_attributes }
        }.to change(Deck, :count).by(1)
      end

      it 'creates a new Deck increment Cards' do
        expect {
          post :create, params: { deck: valid_attributes }
        }.to change(Card, :count).by(2)
      end

      it 'renders a JSON response with the new deck' do
        post :create, params: { deck: valid_attributes }
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(deck_url(Deck.last))
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new deck' do
        post :create, params: { deck: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        {
          name: 'Spanish Updated'
        }
      end

      it 'updates the requested deck' do
        # print valid_attributes
        deck = Deck.create! valid_attributes
        put :update, params: { id: deck.to_param, deck: new_attributes }
        deck.reload
        expect(deck.name).to eq 'Spanish Updated'
      end

      it 'renders a JSON response with the deck' do
        deck = Deck.create! valid_attributes

        put :update, params: { id: deck.to_param, deck: valid_attributes }
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the deck' do
        deck = Deck.create! valid_attributes

        put :update, params: { id: deck.to_param, deck: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested deck' do
      deck = Deck.create! valid_attributes
      expect {
        delete :destroy, params: { id: deck.to_param }
      }.to change(Deck, :count).by(-1)
    end
  end

end
