# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Card, type: :model do
  it { should have_many(:decks).through(:deck_cards) }
  it { should validate_presence_of :word }
  it { should validate_uniqueness_of(:word).case_insensitive }
  it { should validate_presence_of :translation }
  # it { should validate_presence_of :picture }


end
