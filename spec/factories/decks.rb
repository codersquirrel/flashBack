FactoryBot.define do
  factory :deck do
    sequence(:name) { |n| "deck#{n}" }
  end

  trait :with_cards do
    after :create do |deck|
      create_list :card, 3, deck: deck
    end
  end
end
