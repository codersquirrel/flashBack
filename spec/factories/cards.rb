FactoryBot.define do
  factory :card do
    sequence(:word) { |n| "card#{n}" }
    translation { "MyString" }
    picture { "MyString" }
    hint { "MyText" }
    # deck { nil }
  end
end
