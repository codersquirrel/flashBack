# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Decks', type: :request do
  describe 'GET api/decks' do
    it 'Should return the list of decks!' do
      get decks_path
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET api/decks/:id' do
    let(:deck) { create :deck }
    it 'Should return the deck of a specific id!' do
      get deck_path deck.id
      expect(response).to have_http_status(200)
    end
  end

  # more tests for other types of requests
end
