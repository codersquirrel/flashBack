# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DeckSerializer, type: :serializer do
  context 'when render index' do
    let(:deck) { create :deck, name: 'Fruit' }
    let(:serializer) { described_class.new(deck) }
    let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

    let(:subject) { JSON.parse(serialization.to_json) }

    it 'includes name attribute' do
      require 'pry'
      binding.pry
      expect(subject['name']).to eq 'Fruit'
    end
  end

  context 'when render show' do
    let(:deck) { create :deck, :with_cards, name: 'Animals' }
    let(:serializer) { described_class.new(deck, show_cards: true) }
    let(:serialization) { ActiveModelSerializers::Adapter.create(serializer) }

    let(:subject) { JSON.parse(serialization.to_json) }

    it 'includes cards attributes' do
      expect(subject['cards'][0]['word']).to eq 'card1'
    end
  end
end


