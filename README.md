# README


**Setup**
* ruby 2.6.5

* rails 5.2.4.1

* @vue/cli 4.1.2


**Installation**

```
git clone https://gitlab.com/codersquirrel/flashBack.git

cd flashBack

bundle install
```


**Creating the Database**

```
rails db:create

rails db:migrate

```

**Starting the server - rails**
```
rails s
```

**Starting the server - vue**
```
cd vue

yarn install

yarn serve
```


**To run the test**

```
rake
```

