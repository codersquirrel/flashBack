import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = "http://localhost:3000/api"
axios.defaults.headers.post['Content-Type'] = 'application/json';
// const apiClient = axios.create({
//   baseURL: process.env.VUE_APP_API_BASE_URL,
//   withCredentials: false,
//   headers: {
//     Accept: 'application/json'
//   },
//   responseType: 'json'
// });
// export default {
//   apiClient
// };

export default new Vuex.Store({
  state: {
    decks: [],
    deck: [],
    cards: [],
    card: [],
  },
  mutations: {
    setDecks(state, decks) {
      state.decks = decks
    },
    setDeck(state, deck) {
      state.deck = deck
    },
    setCards(state, cards) {
      state.cards = cards
    },
    setCard(state, card) {
      state.card = card
    },
    // doesn't work for some reason
    // eraseCard(state, cardId) {
    //   let cards = state.cards.filter(c => c.id != cardId)
    //   state.cards = cards
    // },
    // setHeaders(state) {
    // state.headers = {
    //   'Content-Type': 'application/json',
    //   'Accept': 'application/json',
    //   }
    // }
  },
  actions: {
    fetchDecks({commit}) {
      axios.get('/decks/')
      .then(response => {
        // console.log(response)
        commit('setDecks', response.data)})
      .catch(err => console.log(err.response))
    },
    getDeck({commit}, id){
      axios.get(`/decks/${id}`)
      .then(response => {
        // console.log(response)
        commit('setDeck', response.data)})
      .catch(err => console.log(err.response))
    },
    postDeck({commit, state}, deck) {
      // commit('setHeaders')
      return new Promise((resolve, reject) => {
        axios.post("/decks", deck, { headers: state.headers })
          .then(response => {
            commit('setDeck', response.data)
            resolve(response)
          })
          .catch(err => {
            reject(err)
            console.log(err)
        })
      })
    },
    editDeck({commit}, deck) {
      return axios.put(`/decks/${deck.id}/`, {deck})
      .then(({data}) => {
        // console.log(data)
        commit('setDeck', data)
        return {data}
        // resolve(response)
      })
      .catch(err => {
        // reject(err)
        console.log(err)
      })
    },
    removeDeck(deck, id) {
      return axios.delete(`/decks/${id}/`)
    },
    fetchCards({commit}) {
      axios.get('/cards')
      .then(response => {
        console.log(`fetchCard response: ${response}`)
        commit('setCards', response.data)})
      .catch(err => console.log(err.response))
    },
    getCard({commit}, id){
      axios.get(`/cards/${id}`)
      .then(response => {
        console.log(response)
        commit('setCard', response.data)})
      .catch(err => console.log(err.response))
    },
    postCard({commit}, card) {
      // commit('setHeaders')
      console.log(card)
      console.log(card.deckIds)
     return axios.post('/cards', {card: card})
       .then(({data}) => {
         console.log(data)
         commit('setCard', data)
         return {data}
         // resolve(response)
       })
       .catch(err => {
         // reject(err)
         console.log(err)
       })
     },
     editCard({commit}, card) {
        console.log(card.id)
        return axios.put(`/cards/${card.id}/`, {card: card})
        .then(({data}) => {
          // console.log(data)
          commit('setCard', data)
          return {data}
          // resolve(response)
        })
        .catch(err => {
          // reject(err)
          console.log(err)
        })
      // let response = axios.put(`/cards/${id}`, card)
      // let newCard = response.data.data.attributes;
      // commit('edited_card', newCard)
     },
      removeCard(card, id) {
        return axios.delete(`/cards/${id}/`)
    },
  }
})

