import Vue from 'vue'
import VueRouter from 'vue-router'

import Navbar from '@/views/dashboard/Navbar'
import Home from '@/views/dashboard/children/home/Index'
import DecksIndex from '@/views/dashboard/children/decks/Index'
import DeckShow from '@/views/dashboard/children/decks/Show'
import DeckNew from '@/views/dashboard/children/decks/New'
import DeckEdit from '@/views/dashboard/children/decks/Edit'
import DeckPlay from '@/views/dashboard/children/decks/Play'
import CardsIndex from '@/views/dashboard/children/cards/Index'
import CardNew from '@/views/dashboard/children/cards/New'
import CardShow from '@/views/dashboard/children/cards/Show'
import CardEdit from '@/views/dashboard/children/cards/Edit'
import Dashboard from '@/views/dashboard/Dashboard'


Vue.use(VueRouter)

const routes = [
  {
    path: '/', component: Navbar,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/home',
        name: 'home',
        component: Home
      },
      {
        path: '/decks',
        name: 'decks',
        component: DecksIndex
      },
      {
        path: '/cards',
        name: 'cards',
        component: CardsIndex
      },
      {
        path: '/decks/new',
        name: 'new_deck',
        component: DeckNew
      },
      {
        path: '/decks/:id',
        name: 'deck',
        component: DeckShow
      },
       {
        path: '/decks/:id/play',
        name: 'play_deck',
        component: DeckPlay
      },
      {
        path: '/decks/:id/edit',
        name: 'edit_deck',
        component: DeckEdit
      },
      {
        path: '/cards/new',
        name: 'new_card',
        component: CardNew
      },
       {
        path: '/cards/:id',
        name: 'card',
        component: CardShow
      },
       {
        path: '/cards/:id/edit',
        name: 'edit_card',
        component: CardEdit
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
