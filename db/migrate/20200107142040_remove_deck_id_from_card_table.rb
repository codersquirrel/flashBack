class RemoveDeckIdFromCardTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :cards, :deck_id, :integer
  end
end
