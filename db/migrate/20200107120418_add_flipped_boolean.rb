class AddFlippedBoolean < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :flipped, :boolean
  end
end
