# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



card1 = Card.create(
  word: "Apple",
  translation: "Manzana",
  hint: "Hint",
  picture: "",
  flipped: false,
)


card2 = Card.create(
  word: "Pear",
  translation: "Birne",
  hint: "Like the light bulb",
  picture: "",
  flipped: false,
)



card3 = Card.create(
  word: "Grape",
  translation: "Uva",
  hint: "Like the light wave",
  picture: "",
  flipped: false,
)

card4 = Card.create(
  word: "Banana",
  translation: "Platano",
  hint: "Like a plant",
  picture: "",
  flipped: false,
)

card5 = Card.create(
  word: "Salad",
  translation: "Lechuga",
  hint: "Starts with an L",
  picture: "",
  flipped: false,
)

card6 = Card.create(
  word: "Tomato",
  translation: "Jitomate",
  hint: "Almost the same",
  picture: "",
  flipped: false,
)

card7 = Card.create(
  word: "Carrot",
  translation: "Zanahoria",
  hint: "Starts with a Z",
  picture: "",
  flipped: false,
)

card8 = Card.create(
  word: "Dog",
  translation: "Perro",
  hint: "Rolling R",
  picture: "",
  flipped: false,
)

card9 = Card.create(
  word: "Cat",
  translation: "gato",
  hint: "Almost like cat with an o at the end",
  picture: "",
  flipped: false,
)

deck1 = Deck.create(
  name: "Fruit",
  cards: [card1, card2, card3, card4],
)


deck2 = Deck.create(
  name: "Vegetables",
  cards: [card5, card6, card7],
)

deck3 = Deck.create(
  name: "Animals",
  cards: [card8, card9],
)
# Card.all.each { |card| card.decks << Deck.all.sample }
