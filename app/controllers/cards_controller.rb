# frozen_string_literal: true

class CardsController < ApplicationController
  before_action :set_card, only: [:show, :update, :destroy]

  def index
    @cards = Card.all

    render json: @cards
  end

  def show
    render json: @card
  end

  def create
    @card = Card.new(card_params.to_h)
    # @card.deck_ids = params[:deck_ids]

    if @card.save
      render json: @card, status: :created, location: @card
    else
      render json: @card.errors, status: :unprocessable_entity
    end
  end

  def update
    if @card.update(card_params)
      render json: @card
    else
      render json: @card.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @card.deck_ids = params[:deck_ids]
    @card.destroy
  end

  private

  def set_card
    @card = Card.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:word, :translation, :picture, :hint, :flipped, :id, deck_ids:[])
    # params.require(:card).permit(:word, :translation, :picture, :hint, :flipped, :deck_ids)

    # params.requite(:card).permit(cards_attributes: [:id, :word, :translation, :_destroy])
    # params.require(:card).permit(:word, :translation, :id)
    # params.permit(:card)

  end
end
