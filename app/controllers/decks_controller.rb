# frozen_string_literal: true

class DecksController < ApplicationController
  before_action :set_deck, only: [:show, :update, :destroy]

  def index
    @decks = Deck.all

    render json: @decks, show_cards: true
  end

  def show
    @deck = Deck.find(params[:id])
    @cards = @deck.cards
    render json: @deck, show_cards: true
  end

  def create
    @deck = Deck.new(deck_params)
    if @deck.save
      render json: @deck, status: :created, location: @deck
    else
      render json: @deck.errors, status: :unprocessable_entity
    end
  end

  def update
    if @deck.update(deck_params)
      render json: @deck
    else
      render json: @deck.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @cards = @deck.cards
    @deck.destroy
  end

  private

  def set_deck
    @deck = Deck.find(params[:id])
  end

  def deck_params
    # :confidence_level
    params.require(:deck).permit(:name, cards_attributes: [:id, :word, :translation, :_destroy])
  end
end
