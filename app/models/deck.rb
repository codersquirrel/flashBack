# frozen_string_literal: true

class Deck < ApplicationRecord
  has_many :deck_cards
  has_many :cards, through: :deck_cards, dependent: :delete_all
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  accepts_nested_attributes_for :cards, allow_destroy: true
end
