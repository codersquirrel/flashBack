class Card < ApplicationRecord
  has_many :deck_cards
  has_many :decks, through: :deck_cards
  has_many :deck_cards_attributes

  validates :word, presence: true, uniqueness: { case_sensitive: false }
  validates :translation, presence: true
  # validates :hint, presence: true
  # validates :picture, presence: true


  # accepts_nested_attributes_for :deck_cards_attributes, allow_destroy: true
  # validates :picture, presence: true
end
