class CardSerializer < ActiveModel::Serializer
  attributes :id, :word, :translation, :picture, :hint, :created_at, :deck_ids, :updated_at
  has_many :decks, through: :deck_card, if: -> { should_show_cards }

  def should_show_cards
    @instance_options[:show_cards]
  end
end
