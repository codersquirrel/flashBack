class DeckSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :cards, through: :deck_card, if: -> { should_show_cards }
  # has_many :cards, if: -> { should_show_cards }

  def should_show_cards
    @instance_options[:show_cards]
  end
end
